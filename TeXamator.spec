Name:           TeXamator
Version:        1.7.5
Release:        11
Summary:        Helping one deal with an exercises database
License:        GPLv3
URL:            http://alexisfles.ch/en/texamator/texamator.html
Source0:        http://alexisfles.ch/files/TeXamator/TeXamator.v.%{version}.tar.gz
Source1:        TeXamator.desktop
BuildArch:      noarch
BuildRequires:  desktop-file-utils
Requires:       tex(latex) dvipng

%description
TeXamator is designed at helping one deal with an exercises database (hence the name (t)exam-ator).
Once configured, it will open all your .tex files in a given directory (recursively) and look for
exercises (or anything you want it to). Then, it will show you everything in a tree and let you
expand/collapse folders.Finally, you will be able to export all the exercises you selected to a
.tex file or to a pdf.

%prep
%autosetup -n %{name} -p1
find -name '*~' -delete

%build

%install
install -d %{buildroot}%{_bindir}
install -d %{buildroot}%{_datadir}/TeXamator
install -p TeXamator.py %{buildroot}%{_datadir}/TeXamator/TeXamator.py
cp -rp partielatormods %{buildroot}%{_datadir}/TeXamator
cp -rp ts_files %{buildroot}%{_datadir}/TeXamator
cp -rp ui_files %{buildroot}%{_datadir}/TeXamator
ln -s %{_datadir}/TeXamator/TeXamator.py %{buildroot}%{_bindir}/TeXamator
sed -i -e '/\/usr\/bin\/python/d' %{buildroot}%{_datadir}/TeXamator/partielatormods/*.py
for lib in %{buildroot}%{_datadir}/TeXamator/partielatormods/*/*.py; do
 sed '/\/usr\/bin\/python/d' $lib > $lib.new && touch -r $lib $lib.new && mv $lib.new $lib
done
desktop-file-install --dir=%{buildroot}%{_datadir}/applications %{SOURCE1}
desktop-file-validate %{buildroot}/%{_datadir}/applications/TeXamator.desktop

%files
%doc README gpl-3.0.txt
%{_bindir}/TeXamator
%{_datadir}/TeXamator
%{_datadir}/applications/TeXamator.desktop
%exclude %{_datadir}/TeXamator/ui_files/plop.sh

%changelog
* Wed Jan 8 2019 Tianfei <tianfei16@huawei.com> - 1.7.5-11
- Package init
